#!/bin/sh

if [ $(id -u) -ne 0 ]; then
        echo "Only root can do this"
        exit 1
fi

# asking username
echo "Type your username:"
read usname

# username.txt
#echo $usname > /home/$usname/username.txt

# Current dir path
path1="$PWD"

# Save the corrent path
echo $path1 > /home/$usname/path1.txt

# Install conky & dependecy packages
apt install conky-all hddtemp lm-sensors

# hddtemp permissin change to use as regular user
#sudo chmod +s /usr/sbin/hddtemp

sudo -u $SUDO_USER bash -c '\
	usname="$(whoami)"

	path1="$(cat ~/path1.txt)"
	rm -f ~/path1.txt

	nmname='$(nmcli |grep en | awk -F' ' 'END{print $2}')'

	mkdir -p ~/.conky

	cd $path1 && chmod +x waitConky.sh

	n=$(nproc)
	for (( i=$n; i>=1; i-- )); do
	    sed -i "64i Core$i \${cpu cpu$i}% \${cpubar cpu$i}\\
Freq: \${freq $i}MHz" '$path1/midnight_conky.conf'
	done

	sed -i "s/Core1 \${cpu cpu1}% \${cpubar cpu1}/\$font\${color DimGray}Core1 \${cpu cpu1}% \${cpubar cpu1}/" '$path1/midnight_conky.conf'

	sed -i "s/enp2s0/$nmname/g" "$path1/midnight_conky.conf"
	
	cp $path1/*.lua $path1/*.conf $path1/waitConky.sh "${HOME}/.conky"
	
#	conky -c ${HOME}/.conky/midnight_conky.conf

	mkdir -p ~/.config/autostart && cp $path1/midnight_conky.desktop ~/.config/autostart

	echo Exec=/home/$usname/.conky/waitConky.sh >> ${HOME}/.config/autostart/midnight_conky.desktop

'
echo
echo "***************************************************************"
echo "*		 Installed Midnight conky theme 	      *"
echo "***************************************************************"
echo