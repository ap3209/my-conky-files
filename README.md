# My-conky-files

conky theme
![](screenshot/conky_hours_minutes_seconds.png)

How to install theme

make sure you have a conky install <br>
$ sudo apt install conky-all hddtemp lm-sensors

$ sudo chmod +s /usr/sbin/hddtemp

$ mkdir -p .conky

$ git clone https://gitlab.com/ap3209/my-conky-files.git && cd my-conky-files && chmod +x waitConky.sh

$ mv *.lua *.conf waitConky.sh conky.desktop "${HOME}/.conky" && cd .. && rm -rf My-conky-files

$ conky -c ${HOME}/.conky/halflifeprince_conky.conf

To start conky automatic on every boot add waitConky.sh file on startup manager and file is located on .conky folder or try below command it should work <br>
$ mkdir -p .config/autostart && cd .conky && mv conky.desktop ${HOME}/.config/autostart && cd ..

$ echo Exec=/home/$USER/.conky/waitConky.sh >> ${HOME}/.config/autostart/conky.desktop
