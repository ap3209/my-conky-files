#!/bin/bash

n=$(nproc)

for (( i=$n; i>=1; i-- )); do
    sed -i "64i Core$i \${cpu cpu$i}% \${cpubar cpu$i}\\
Freq: \${freq $i}MHz" tt1.conf
done

sed -i 's/Core1 ${cpu cpu1}% ${cpubar cpu1}/$font${color DimGray}Core1 ${cpu cpu1}% ${cpubar cpu1}/' tt1.conf


#!/bin/bash
#
#n=$(nproc)
#
#for (( i=$n; i>=1; i-- )); do
#    sed -i '65i Core$(echo $i) ${cpu cpu$(echo $i)}% ${cpubar cpu$(echo $i)}\
#Freq: ${freq $(echo $i)}MHz' tt1.conf
#done