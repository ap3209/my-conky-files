gap_x 10
gap_y 60



${font Arial:bold:size=10}${color #3b71a1}HDD ${color #3b71a1}${hr 2}
$font${color DimGray}/home $alignc ${fs_used /home} / ${fs_size /home} $alignr ${fs_free_perc /home}%
${fs_bar /home}
${color #3b71a1}Disk I/O: $color${diskio}


${font Arial:bold:size=10}${color #3b71a1}MEMORY ${color #3b71a1}${hr 2}
$font${color DimGray}MEM $alignc $mem / $memmax $alignr $memperc%
$membar


/dev/sda: ${execi 1 hddtemp /dev/sda|sed 's%/dev/sda: %%' }

${execi 1 hddtemp /dev/sdb|sed 's%/dev/sda: %%' }

${alignr 10}${color}SSD M500 Crucial 120GB Temp ${color1}${exec hddtemp /dev/sda | awk '{print $NF}'}

@P5KPL-AM-PS:~$ sensors |grep 'CPU Temperature' |cut -d "+" -f2 
37.0°C  (high = 
@P5KPL-AM-PS:~$ sensors |grep 'CPU Temperature' |cut -d "+" -f2| cut -c1-6
37.0°
@P5KPL-AM-PS:~$ sensors |grep 'CPU Temperature' |cut -d "+" -f2| cut -c1-6
37.0°
@P5KPL-AM-PS:~$ sensors |grep 'CPU Temperature' |cut -d "+" -f2| cut -1-6
cut: invalid option -- '1'
Try 'cut --help' for more information.
@P5KPL-AM-PS:~$ sensors |grep 'CPU Temperature' |cut -d "+" -f2| cut -c1-6
37.0°
@P5KPL-AM-PS:~$ sensors |grep 'CPU Temperature' |cut -d "+" -f2 
36.0°C  (high = 
@P5KPL-AM-PS:~$ man cut

hddtemp /dev/sda | cut -c 28-32

lm-sensors

${font Arial:bold:size=10}${color #3b71a1}TEMPERATURES ${color #3b71a1}${hr 2}
$font${color DimGray}TOSHIBA HDWD110: ${color3}${exec hddtemp /dev/sda |cut -c 28-32}
$font${color DimGray}TOSHIBA HDWU110: ${color3}${exec hddtemp /dev/sdb |cut -c 28-32}
$font${color DimGray}CPU TEMPERATURE:  ${color3}${exec sensors |grep 'CPU Temperature' |cut -d "+" -f2|cut -c 1-2,5-7}
$font${color DimGray}MB TEMPERATURE:   ${color3}${exec sensors |grep 'MB Temperature' |cut -d "+" -f2|cut -c 1-2,5-7}
$font${color DimGray}GRAPHIC CARD:          ${color3}${exec nvidia-smi |grep 'Default'|cut -c 9-10}°C
